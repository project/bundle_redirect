<?php

namespace Drupal\bundle_redirect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\NodeType;

/**
 * Provides settings for bundle redirect module.
 */
class BundleRedirectSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['bundle_redirect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bundle_redirect_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bundle_redirect.settings');

    $options = [];
    foreach (NodeType::loadMultiple() as $bundle) {
      $options[$bundle->id()] = $bundle->label();
    }

    if (empty($options)) {
      $this->messenger()->addWarning($this->t('No node bundles found. @link', [
        '@link' => Link::createFromRoute($this->t('+ Create a new bundle'), 'node.type_add')->toString(),
      ]));
      return [];
    }

    $form['node_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node bundles'),
      '#description' => $this->t('Enable redirect for selected node bundles.'),
      '#options' => $options,
      '#default_value' => $config->get('node_bundles') ?? [],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config = $this->config('bundle_redirect.settings');
    $config->set('node_bundles', array_filter($values['node_bundles']));
    $config->save();

    $this->messenger()->addMessage($this->t('Configs have been saved.'));
  }

}
