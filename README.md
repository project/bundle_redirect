# Bundle redirect

The Bundle redirect module provides a simple additional feature to redirect
module https://www.drupal.org/project/redirect. It allows the user to add
redirect from a node. This is similar to the Drupal 7 feature provided by the
redirect module.

The redirects have to be added for a node from the admin page
admin/config/search/redirect on Drupal 8. This module helps to overcome the
issue, by allowing all the redirect operation from the node itself. It works
currently only with node. It could be extended easily to other entities like
taxonomy.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bundle_redirect).

To submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/bundle_redirect).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following module:

- Redirect (https://www.drupal.org/project/redirect)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

 - Navigate to Administration > Extend and enable the module.
 - Enable content bundles on /admin/config/search/bundle-redirect page.
 - Upon saving a node, there is now a vertical tab for URL redirects in the
      advanced settings on node edit form.


## Maintainers

- Chandra Rajakumar - [chandraraj](https://www.drupal.org/u/chandraraj)
- Alex Burrows - [aburrows](https://www.drupal.org/u/aburrows)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
